const express = require('express');
const router = express.Router();
const Documento = require('../models/documento');

// Devuelve la cantidad de documentos que hay en la base de datos, para definir la cantidad de botones en la paginacion
router.get('/totalfilas',(req,res,next)=>{
    Documento.find({},(err,data)=>{
        if(err){ console.error(err); } else {
            console.log(data);
            res.json(data);
        }
    }).countDocuments();
});

// Devuelve la informacion detallada de un documento
router.get('/:id',(req,res)=>{
    Documento.find({ _id:req.params.id },(err,data)=>{
        if(err){ console.error(err);}else{
            res.json(data);
        }
    });
});

// Modifica un documento por id
router.put('/:id',(req,res)=>{
    console.log(req.body);
    Documento.updateOne({ _id:req.params.id },req.body,(err,data)=>{
        res.redirect('/');
    });
});

// Consulta la informacion de un documento y despues renderiza el template del formulario
router.get('/editar/:id',(req,res)=>{
    Documento.findById({ _id:req.params.id },(err,data)=>{
        console.log(data);
        res.render('editar',{documento:data});
    });
});

// Consulta las filas de la tabla dependiendo de la paginacion
router.get('/:indicepaginacion/:filasporpagina',(req, res)=>{
    var indicepaginacion = Number.parseInt(req.params.indicepaginacion);
    var filasporpagina = Number.parseInt(req.params.filasporpagina);
    indicepaginacion = indicepaginacion*filasporpagina;
    Documento.find({},(err,documentos)=>{
        res.json(documentos);
    }).sort({ title:1, poster_path:1 }).skip(indicepaginacion).limit(filasporpagina);
});

// Para crear un documento y grabarlo
router.post('/',(req, res)=>{
    console.log('llegue bien');
    delete req.body._id;
    console.log(req.body);
    Documento.create(req.body, (err,documento)=>{
        if(err){ console.error(err); } else {
            res.json(documento);
        }
    });
});

// Para eliminar un documento
router.delete('/:id',(req,res)=>{
    console.log(req.params.id);
    Documento.deleteOne({ _id:req.params.id },(err,data)=>{
        if(err){ console.error(err); } else {
            res.json(data);
        }
    });
});

module.exports = router;