const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let DocumentoSchema = Schema({
    title: { type: String,            required: true},
    poster_path: { type: String,      required: false},
    vote_average: { type: Number,     required: false},
    vote_count: { type: Number,       required: false},
    popularity: { type: Number,       required: false},
    release_date: { type: String,     required: false},
    original_language: { type: String,required: false},
    original_title: { type: String,   required: false},
    overview: { type: String,         required: false},      
});

module.exports = mongoose.model('Documento',DocumentoSchema);