import { Component, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { DocumentoService } from './documento.service';
import { Documento } from './documento';
import { Paginacion } from './paginacion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-crud';
  documentos: Documento[] = [];
  totalfilas: number;
  indicepaginacion: number = 0;
  filasporpagina: number = 3;
  paginacion: Paginacion = new Paginacion(this.indicepaginacion,this.filasporpagina);

  // En el constructor definimos un atributo que proporciona los metodos del servicio
  constructor(
    private serviciodocumento: DocumentoService
  ) { }

  ngOnInit(): void {
    this.obtenerdocumentos(); // Consultamos los documentos que se mostraran en la tabla
    this.obtenertotalfilas(); // Consultamos la cantidad de documentos en la colleccion para definir la cantidad de botones
    this.paginacion = new Paginacion(this.indicepaginacion,this.filasporpagina);
  }

  ngOnChange(changes: SimpleChange){
    this.paginacion = new Paginacion(this.indicepaginacion,this.filasporpagina);
  }

  // Cambia el indice de paginacion y vuelve a consultar los documentos
  cambiarindicepag(nuevo_indice: number){
    this.indicepaginacion = nuevo_indice;
    this.obtenerdocumentos();
  }

  // Consulta las filas de la tabla
  obtenerdocumentos(){
    this.paginacion = new Paginacion(this.indicepaginacion,this.filasporpagina);
    this.serviciodocumento.obtenerdocumentos(this.paginacion)
    .then(documentos => this.documentos = documentos)
    .catch(err => console.log(err));
  }

  // Consulta la cantidad de documentos en la colleccion
  obtenertotalfilas(){
    this.serviciodocumento.obtenertotalfilas()
    .then(total => this.totalfilas = total)
    .catch(err => console.log(err));
  }

  eliminardocumento(documento: Documento){
    console.log("LLegue a la funcion que llama al servicio");
    console.log(documento);
    this.serviciodocumento.eliminar(documento)
    .then(status => {this.obtenerdocumentos();this.obtenertotalfilas();})
    .catch(err => console.error(err));
  }

  creardocumento(documento: Documento){
    console.log("LLegue al componente app");
    console.log(documento);
    this.serviciodocumento.crear(documento)
    .then(status => {this.obtenerdocumentos();this.obtenertotalfilas();})
    .catch(err => console.log(err));
  }
}
