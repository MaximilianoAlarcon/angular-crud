import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Documento } from '../documento';

@Component({
  selector: 'app-lista-documento',
  templateUrl: './lista-documento.component.html',
  styleUrls: ['./lista-documento.component.scss']
})
export class ListaDocumentoComponent implements OnInit {

  constructor() { }

  @Input() indicepaginacion: number;
  @Input() filasporpagina: number;
  @Input() totalfilas: number;
  @Input() documentos: Documento[];
  @Output() eventoeliminardocumento: EventEmitter<any> = new EventEmitter();
  @Output() consultartotalfilas: EventEmitter<any> = new EventEmitter();
  @Output() cambiarindicepag: EventEmitter<any> = new EventEmitter();
  @Output() cambiarfilasporpag: EventEmitter<any> = new EventEmitter();

  /* Cantidad de botones en la paginacion */
  cantidadbotones: number;
  /* counter es un atributo en forma de array que despues se le asigna un elemento pero n cantidad de veces,
  donde n es la cantidad de botones. Esto sirve solo para poder recorrer un ciclo n cantidad de veces y
  escribir los botones
  */
  counter = Array();

  ngOnInit(): void {
    
    this.cantidadbotones = Math.ceil((this.totalfilas / this.filasporpagina));
    // Le puse esta validacion porque al iniciar la pagina los atributos totalfilas y filasporpagina
    // tienen que esperar a recibir los datos
    if(!isNaN(this.cantidadbotones)) 
    {
      this.counter = [].constructor(this.cantidadbotones);
    }
  }

  ngOnChanges(changes: SimpleChange){
    this.cantidadbotones = Math.ceil((this.totalfilas / this.filasporpagina));
    if(!isNaN(this.cantidadbotones))
    {
      this.counter = [].constructor(this.cantidadbotones);
    }
  }

  // Primero pregunto si esta seguro de eliminar, y en tal caso emite la salida para que lo reciba el componente principal
  eliminar(documento: Documento){
    let confirmacion = confirm("Estas seguro/a?");
    console.log(documento);
    if(confirmacion){ this.eventoeliminardocumento.emit(documento); }
  }

  // Al presionar el boton anterior, disminuye en uno el indice de paginacion y lo emite
  anterior(){
    if(this.indicepaginacion >= 1)
    {
    this.indicepaginacion -= 1;
    this.cambiarindicepag.emit(this.indicepaginacion);
    }
  }
  // Al presionar el boton siguiente, aumenta en uno el indice de paginacion y lo emite
  siguiente(){
    if(this.indicepaginacion < (this.cantidadbotones - 1))
    {
    this.indicepaginacion += 1;
    this.cambiarindicepag.emit(this.indicepaginacion);
    }
  }
  // Para poder elegir un indice de paginacion
  cambiarindice(indice: number){
    this.indicepaginacion = indice;
    this.cambiarindicepag.emit(this.indicepaginacion);
  }
}
