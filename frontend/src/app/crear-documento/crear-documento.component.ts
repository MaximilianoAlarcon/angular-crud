import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Documento } from '../documento';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-crear-documento',
  templateUrl: './crear-documento.component.html',
  styleUrls: ['./crear-documento.component.scss']
})
export class CrearDocumentoComponent implements OnInit {

  // Practica de validacion con formBuilder
  formulario = this.fb.group({
    title: ['', Validators.required],
    poster_path: [''],
    vote_average: [''],
    vote_count: [''],
    popularity: [''],
    release_date: [''],
    original_language: [''],
    original_title: [''],
    overview: [''],
    aliases: this.fb.array([
      this.fb.control('')
    ])
  });
  constructor(private fb: FormBuilder) { }

  @Output() creardocumento: EventEmitter<any> = new EventEmitter();
  documento = new Documento;

  ngOnInit(): void {
  }

  /*
  Cuando el usuario oprime "Enviar", activa el evento de salida y con la info del documento
  como parametro. Este evento es recibido por app component, es decir, el componente principal y
  finalmente ejecuta el metodo crear() del servicio DocumentoService para hacer la peticion
  */
  submitcreardocumento(){
    console.log(this.documento);
    this.creardocumento.emit(this.documento);
  }
}
