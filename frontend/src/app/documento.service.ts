import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Documento } from './documento';
import "rxjs";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Paginacion } from './paginacion';

@Injectable({
  providedIn: 'root'
})

export class DocumentoService {

  constructor(
    private _http: Http
  ) { }

  crear(documento: Documento){
    return this._http.post('/', documento)
    .map(data => data.json()).toPromise();
  }

  eliminar(documento: Documento){
    return this._http.delete('/'+documento._id)
    .map(data => data.json()).toPromise();
  }
  obtenerdocumentos(paginacion: Paginacion){
    return this._http.get('/' + paginacion.indicepaginacion + '/' + paginacion.filasporpagina)
    .map(data => data.json()).toPromise();
  }

  obtenerdocumento(documento: Documento){
    return this._http.get('/'+documento._id)
    .map(data => data.json()).toPromise();
  }

  obtenertotalfilas(){
    return this._http.get('/totalfilas')
    .map(data => data.json()).toPromise();
  }
}
