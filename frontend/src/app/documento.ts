export class Documento {
    constructor(
        public _id: number = Math.floor(Math.random()*1000),
        public title: string = "",
        public poster_path: string = "",
        public vote_average: number = 0,
        public vote_count: number = 0,
        public popularity: number = 0,
        public release_date: string = "",
        public original_language: string = "",
        public original_title: string = "",
        public overview: string = ""      
    ){}
}
