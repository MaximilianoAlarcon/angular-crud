export class Paginacion {
    constructor(
        public indicepaginacion: number,
        public filasporpagina: number
    ){}
}