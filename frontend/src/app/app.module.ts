import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DocumentoService } from './documento.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrearDocumentoComponent } from './crear-documento/crear-documento.component';
import { ListaDocumentoComponent } from './lista-documento/lista-documento.component';

@NgModule({
  declarations: [
    AppComponent,
    CrearDocumentoComponent,
    ListaDocumentoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    DocumentoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
