const express = require('express'); //Para manejar mas facil el servidor
const path = require('path');
const bodyparser = require('body-parser'); //Para capturar el cuerpo de una peticion
//const morgan = require('morgan');
const mongoose = require('mongoose'); //Para la conexion a mongodb
const apiRouter = require('./routes/api'); //Enrutador
const methodOverride  = require('method-override'); //Para poder definir el metodo de una peticion en el formulario de edicion
require('dotenv').config({ path: '.env' });
const app = express();

//Configuracion

app.set('view engine','ejs'); //Para poder hacer interpolacion entre el html y el resultado de consulta en mongodb
app.set('views', __dirname);
app.use(methodOverride('_method'));

//Conexion a mongodb
mongoose.connect(process.env.MONGODB_URI,{ useNewUrlParser: true,useUnifiedTopology: true },(err,db)=>{
    if(err){ console.error(err) } else {
        console.log("Conectado a MongoDB");
    }
});

//Morgan para ver por consola las peticiones hechas al servidor
//app.use(morgan('dev'));

//Bodyparser para visualizar en general los datos que se transmiten en las peticiones
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));

//rutas

app.use(express.static(path.join(__dirname,'frontend/dist/angular-crud'))); //Por defecto renderizara el frontend de angular

app.use('/',apiRouter);

//Iniciando el servidor

var port = (process.env.PORT || 3000);
app.set('port',port);

app.listen(app.get('port'), ()=>{
    console.log("Servidor funcionando en puerto "+app.get('port'));
});
